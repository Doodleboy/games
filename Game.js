var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
var laserctx = canvas.getContext('2d');
canvas.width = 800;
canvas.height = 600;
var mySprite = {
    x: 200,
    y: 200,
    width: 50,
    height: 50,
    speed: 200,
    color: 'yellow'
};
var laser = {
	x: 200,
	y: 200,
	width: 25,
	height: 100,
	speed: 200,
	color: '#c00'
}
var enemy = {
	x: 100,
	y: 100,
	width: 50,
	height: 50,
	speed: 200,
	color: '#c00'
}
var mySpritex = mySprite.x;
var mySpritey = mySprite.y;
var laserx = laser.x;
var lasery = laser.y;
var enemyx = enemy.x;
var enemyy = enemy.y;
var keysDown = {};
window.addEventListener('keydown', function(e) {
    keysDown[e.keyCode] = true;
});
window.addEventListener('keyup', function(e) {
    delete keysDown[e.keyCode];
});
 
function update(mod) {
    if (37 in keysDown) {
        mySprite.x -= mySprite.speed * mod;
    	laser.x = mySprite.x
    }
    if (38 in keysDown) {
        mySprite.y -= mySprite.speed * mod;
    	laser.y = mySprite.y
    }
    if (39 in keysDown) {
        mySprite.x += mySprite.speed * mod;
    	laser.x = mySprite.x
    }
    if (40 in keysDown) {
    	mySprite.y += mySprite.speed * mod;
    	laser.y = mySprite.y
    }
}
function render() {
    ctx.fillStyle = '#000';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = mySprite.color;
    ctx.fillRect(mySprite.x, mySprite.y, mySprite.width, mySprite.height);
}
function laser_render() {
    ctx.fillStyle = laser.color;
    ctx.fillRect(laser.x, laser.y, laser.width, laser.height);
}
function enemy_render() {
	ctx.fillStyle = enemy.color;
	ctx.fillRect(enemy.x, enemy.y, enemy.width, enemy.height);
}
function run() {
    update((Date.now() - time) / 1000);
    render();
    laser_render();
    enemy_render();
    time = Date.now();
}
 
var time = Date.now();
setInterval(run, 10);
if (enemyx  == laserx){
	if(enemyy == lasery) {
		confirm("Congragulations! You have killed the enemy!");
	}
}
if (mySpritex == enemyx){
	if(mySpritey == enemyy){
		confirm("Sorry, You have died...");
	}
} 
